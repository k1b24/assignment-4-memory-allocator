//
// Created by Сергей Лазеев on 09.12.2022.
//

#include "tests.h"

int main() {
    int test_counter = 0;
    if (memory_allocate_test()) {
        test_counter++;
    } else {
        printf("memory allocate test error");
    }
    if (memory_free_with_one_block_test()) {
        test_counter++;
    } else {
        printf("memory free with one block test failed");
    }
    if (memory_free_with_two_block_test()) {
        test_counter++;
    } else {
        printf("memory free with two block test failed");
    }
    if (grow_heap_after_another_heap_test()) {
        test_counter++;
    } else {
        printf("grow heap after another heap test failed");
    }
    if (grow_heap_after_with_region_skip_test()) {
        test_counter++;
    } else {
        printf("grow heap after another heap with region skipped test failed");
    }
    if (test_counter == 5) {
        printf("All tests passed!");
    } else {
        printf("Some errors occurred");
    }
    return 0;
}