#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <sys/mman.h>

struct block_header get_header_from_void(void* allocated_memory);

bool memory_allocate_test();

bool memory_free_with_one_block_test();

bool memory_free_with_two_block_test();

bool grow_heap_after_another_heap_test();

bool grow_heap_after_with_region_skip_test();

#endif //MEMORY_ALLOCATOR_TESTS_H
