
#define _DEFAULT_SOURCE

#include "tests.h"

struct block_header get_header_from_void(void* allocated_memory) {
    return (struct block_header){allocated_memory - offsetof(struct block_header, contents)};
}

bool memory_allocate_test() {
    printf("-----------------\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (heap == NULL) {
        return false;
    }

    debug_heap(stdout, heap);

    void *data = _malloc(1000);
    debug_heap(stdout, heap);

    if (data == NULL) {
        return false;
    }

    _free(data);
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);

    return true;
}

bool memory_free_with_one_block_test() {
    printf("-----------------\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (heap == NULL) {
        return false;
    }

    debug_heap(stdout, heap);

    void* first_block = _malloc(500);
    void* second_block = _malloc(500);
    void* third_block = _malloc(500);

    if (first_block == NULL || second_block == NULL || third_block == NULL) {
        return false;
    }

    debug_heap(stdout, heap);

    struct block_header second_header = get_header_from_void(second_block);
    struct block_header third_header = get_header_from_void(third_block);

    _free(second_block);

    if (!second_header.is_free || third_header.is_free) {
        return false;
    }

    _free(first_block);
    _free(third_block);

    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);

    return true;
}

bool memory_free_with_two_block_test() {
    printf("-----------------\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (heap == NULL) {
        return false;
    }

    debug_heap(stdout, heap);

    void* first_block = _malloc(500);
    void* second_block = _malloc(500);
    void* third_block = _malloc(500);

    if (first_block == NULL || second_block == NULL || third_block == NULL) {
        return false;
    }

    debug_heap(stdout, heap);

    struct block_header second_header = get_header_from_void(second_block);
    struct block_header third_header = get_header_from_void(third_block);

    _free(second_block);
    _free(third_block);

    if (!second_header.is_free || !third_header.is_free) {
        return false;
    }

    _free(first_block);

    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);

    return true;
}

bool grow_heap_after_another_heap_test() {
    printf("-----------------\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (heap == NULL) {
        return false;
    }

    debug_heap(stdout, heap);

    void* first_block = _malloc(REGION_MIN_SIZE * 2);
    struct block_header first_header = get_header_from_void(first_block);
    if (first_header.capacity.bytes <= REGION_MIN_SIZE) {
        return false;
    }
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE * 2}).bytes);
    return true;
}

bool grow_heap_after_with_region_skip_test() {
    printf("-----------------\n");
    void* heap = heap_init(REGION_MIN_SIZE);
    if (heap == NULL) {
        return false;
    }
    debug_heap(stdout, heap);
    void* not_free_memory = mmap(heap + REGION_MIN_SIZE, REGION_MIN_SIZE, REGION_MIN_SIZE, PROT_READ | PROT_WRITE,
         MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1);
    void* second_block = _malloc(REGION_MIN_SIZE);
    debug_heap(stdout, heap);


    if (second_block == NULL) {
        return false;
    }
    munmap(heap, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    munmap(not_free_memory, size_from_capacity((block_capacity) {.bytes = REGION_MIN_SIZE}).bytes);
    return true;
}